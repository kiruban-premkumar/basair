module Teams
  class Update
    include Teams::Shared

    def initialize(team, team_params)
      @team = team
      @team_params = team_params
    end

    def self.call(team, team_params)
      new(team, team_params).send(:perform)
    end

    private

    attr_reader :team, :team_params

    def perform
      team.attributes = team_params.except('days')
      days_of_the_week
      convert_zone_times_to_utc
      Webhooks::Github::Manage.(team)
      team.save!
    rescue ActiveRecord::RecordInvalid => e
      OpenStruct.new(success?: false, team: team, error: e.message)
    else
      OpenStruct.new(success?: true, team: team)
    end
  end
end

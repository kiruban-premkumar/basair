module Teams
  class Create
    include Teams::Shared

    def initialize(team, account, team_params, auth_lambda)
      @team = team
      @account = account
      @team_params = team_params
      @auth_lambda = auth_lambda
    end

    def self.call(team, account, team_params, auth_lambda)
      new(team, account, team_params, auth_lambda).send(:perform)
    end

    private

    attr_reader :team, :account, :team_params, :auth_lambda

    def perform
      team.account_id = account.id
      team.days = days_of_the_week
      convert_zone_times_to_utc
      auth_lambda.call
      Webhooks::Github::Manage.(team)
      team.save!
    rescue ActiveRecord::RecordInvalid => e
      OpenStruct.new(success?: false, team: team, error: e.message)
    else
      OpenStruct.new(success?: true, team: team)
    end

  end
end

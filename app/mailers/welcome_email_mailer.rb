class WelcomeEmailMailer < ApplicationMailer
  default from: "'Standup App' <welcome@app.buildasaasappinrails.com>"

  def welcome_email(user)
    @user = user
    mail(to: @user.email, subject: "Welcome to Standup App, #{user.name}!!")
  end
end

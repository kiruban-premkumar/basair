class ApplicationMailer < ActionMailer::Base
  default from: "'Standup App' <Standups@app.buildasaasappinrails.com>"
  layout 'mailer'
end

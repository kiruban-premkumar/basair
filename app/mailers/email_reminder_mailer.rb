class EmailReminderMailer < ApplicationMailer
  def reminder_email(user, team)
    @user = user
    @team = team
    @events =
      Event
      .where(
        team_id: @team.id,
        event_time: 24.hours.ago..Time.now
      )
      .includes([:user,:team])
      .order('event_time DESC')
    reply_to = "'Standup App' <#{'development.' if Rails.env.development?}\
    standup.#{@user.hash_id}@app.buildasaasappinrails.com>"
    mail(
      to: @user.email,
      subject: "#{team.name} Stanup Reminder!",
      reply_to: reply_to
    )
  end
end

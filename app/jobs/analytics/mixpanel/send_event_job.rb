module Analytics
  module Mixpanel
    class SendEventJob < ApplicationJob

      def perform(user, event_name, event_data)
        TRACKER.track(user.hash_id, event_name, event_data)
      end
    end
  end
end

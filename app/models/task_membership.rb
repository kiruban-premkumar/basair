class TaskMembership < ApplicationRecord
  belongs_to :task, optional: true
  belongs_to :standup, optional: true
end

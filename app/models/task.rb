class Task < ApplicationRecord
  # belongs_to :user
  # validates_presence_of  :user

  validates_presence_of :title
  validates_presence_of :type

  has_many :task_memberships, dependent: :destroy
  has_many :standups, through: :task_memberships

  after_commit { standups.find_each(&:touch) }
end

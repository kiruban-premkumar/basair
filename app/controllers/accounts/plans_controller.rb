class Accounts::PlansController < ApplicationController
  skip_before_action :authenticate_user!
  layout 'devise'

  def index
    @plans = Plan.where(active: true, displayable: true)
  end

  def show
    redirect_to(
      new_user_registration_path(plan: params[:id])
    )
  end
end

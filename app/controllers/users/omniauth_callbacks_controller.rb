class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def github
    @auth = request.env['omniauth.auth']
    if successful_integration_creation
      redirect_to(
        integrations_path,
        notice: 'Github integration has been added'
      )
    else
      redirect_to(
        integrations_path,
        alert: "Github was unable to add integration.\
Contact support if this issue persists."
      )
    end
  end

  private

  def create_github_integration
    Integrations::Github.create!(
      account_id: current_account.id,
      settings: {
        token: @auth.credentials.token
      }
    )
  end

  def successful_integration_creation
    @auth != :invalid_credentials &&
      @auth&.credentials&.token &&
      create_github_integration
  end
end

class TeamsController < ApplicationController
  include Limits

  load_and_authorize_resource find_by: :hash_id, except: [:create]
  around_action :use_time_zone, only: [:edit, :show, :standups]
  before_action :setup_integration_data, only: [:new, :edit]

  def index
    @teams = visible_teams
  end

  def show
    set_teams_and_standups(Date.today.iso8601)
    set_events
  end

  def standups
    set_teams_and_standups(current_date)
    set_events
  end

  def new
    @team = Team.new
    set_users
  end

  def create
    check_resource_against_limits(:teams) do
      return redirect_back(
        fallback_location: root_path,
        notice: "You do not have the resources to create this Team,\
 please consider upgrading your plan."
      )
    end

    @team = Team.new(team_params.except('days'))
    response = Teams::Create.(
      @team,
      current_account,
      team_params,
      -> { authorize!(:create, @team) }
    )

    if response.success?
      redirect_to @team, notice: 'Team was successfully created.'
    else
      set_users
      render :new
    end
  end

  def edit
    @team = Team.friendly.find(params[:id])
    # convert_from_utc
    set_users
  end

  def update
    @team = Team.friendly.find(params[:id])
    response = Teams::Update.(@team, team_params)
    if response.success?
      redirect_to teams_url, notice: 'Team was successfully updated.'
    else
      set_users
      setup_integration_data
      render :edit
    end
  end

  def destroy
    @team = Team.friendly.find(params[:id])
    @team.integration_settings['github_repos'] = nil
    Webhooks::Github::Manage.(@team)
    @team.destroy
    redirect_to teams_url, notice: 'team was successfully destroyed.'
  end

  private

  def team_params
    params.require(:team).permit(
      :name, :description, :timezone, :has_reminder,
      :has_recap, :reminder_time, :recap_time, days: [], user_ids: [],
      integration_settings: [:github_collect_events, github_repos: []]
    )
  end

  def set_teams_and_standups(date)
    @team = Team.friendly.includes(:users).find(params[:id])
    @standups = @team.users.flat_map do |u|
      u.standups.where(standup_date: date)
      .includes(:dids, :todos, :blockers)
      .references(:tasks)
    end
  end

  def set_users
    @account_users ||=
      current_account.users.where.not(invitation_accepted_at: nil) +
      current_account.users.with_role(:admin, current_account).distinct
  end

  def set_events
    @events =
      @team
      .events
      .where(
        event_time: (
          current_date.to_time.beginning_of_day..current_date.to_time.end_of_day
        )
      )
      .includes([:user,:team])
      .order('event_time DESC')
  end

  def use_time_zone(&block)
    Time.use_zone(@team.timezone, &block)
  end

  def setup_integration_data
    setup_github
  end

  def setup_github
    @github = Integrations::Github.find_by(account_id: current_account.id)
    return unless @github
    client =  Github.new oauth_token: @github.settings['token']
    # auto_pagination: true
    repos = Rails.cache.fetch(
      "github.repos.#{current_account.hash_id}",
      expires_in: 15.minutes
    ) do
      client.repos.list.body
    end

    @grepos = repos.map do |r|
      OpenStruct.new(name: r.name, owner: r.owner.login)
    end
  end
end

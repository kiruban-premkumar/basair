class PlansController < ApplicationController
  def index
    @plans = Plan.where(active: true, displayable: true)
  end

  def show
    return if handle_free_plan_selected
    return if handle_token_presence
    @plan = Plan.find(params[:id])
    @subscription = Subscription.new
  end

  private

  def handle_free_plan_selected
    return false unless params[:id] == 'free'
    response =
      if current_subscription && current_subscription.plan_id
        stripe_subscription_update_service(token: false)
      else
        stripe_subscription_creation_service
      end
    handle_response(
      response: response,
      success_message: 'Plan was updated successfully.',
      failure_message: 'Plan was unable to be updated!'
    )
  end

  def handle_token_presence
    return false unless current_subscription.stripe_token
    response = stripe_subscription_update_service(token: false)
    handle_response(
      response: response,
      success_message: 'Plan was updated successfully.',
      failure_message: 'Plan was unable to be updated!'
    )
  end

  def stripe_subscription_creation_service
    PaymentServices::Stripe::Subscription::CreateSubscription.(
      customer: current_subscription.stripe_customer_id,
      account: current_account,
      plan: params[:id]
    )
  end

  def stripe_subscription_update_service(token:)
    options = {
      account: current_account,
      plan: params[:id]
    }
    options[:token] = token if token
    PaymentServices::Stripe::Subscription::UpdateSubscription.(
      options
    )
  end

end

class IntegrationsController < ApplicationController
  def index
    @github = Integrations::Github.find_by(account_id: current_account.id)
    return unless @github
    client =  Github.new oauth_token: @github.settings['token']
    repos = Rails.cache.fetch(
      "github.repos.#{current_account.hash_id}",
      expires_in: 15.minutes
    ) do
      client.repos.list.body
    end
    @grepos = repos.map(&:name)
  end

  def destroy
    if integration&.find_by(account_id: current_account.id)&.delete
      redirect_to(
        integrations_path,
        notice: "Removed #{params[:provider].capitalize} integration. Please be\
sure to remove the application through your settings area in \
#{params[:provider].capitalize}"
      )
    else
      redirect_to(
        integrations_path,
        notice: "Unable to removed #{params[:provider].capitalize} integration"
      )
    end
  end

  def integration
    case params[:provider]
    when 'github'
      Integrations::Github
    end
  end
end

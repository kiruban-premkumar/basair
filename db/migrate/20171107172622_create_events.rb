class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :type
      t.references :team, foreign_key: true
      t.string :user_name
      t.references :user, foreign_key: true
      t.string :event_name
      t.text :event_body
      t.string :event_id
      t.jsonb :event_data, null: false, default: {}
      t.datetime :event_time

      t.timestamps
    end

    add_index :events, :event_data, using: :gin
    add_index :events, :type
    add_index :events, :user_name
    add_index :events, :event_id
  end
end

class CreateIntegrations < ActiveRecord::Migration[5.0]
  def change
    create_table :integrations do |t|
      t.references :account, index: true, foreign_key: true
      t.string :type
      t.jsonb :settings, null: false, default: {}

      t.timestamps
    end

    add_index :integrations, :settings, using: :gin
  end
end

class AddHashIdToStandups < ActiveRecord::Migration[5.0]
  def change
    add_column :standups, :hash_id, :string
  end
end

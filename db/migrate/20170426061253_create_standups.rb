class CreateStandups < ActiveRecord::Migration[5.0]
  def change
    create_table :standups do |t|
      t.references :user, foreign_key: true
      t.date :standup_date

      t.timestamps
    end
  end
end

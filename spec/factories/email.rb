FactoryBot.define do
  factory :email, class: OpenStruct do
    # Assumes Griddler.configure.to is :hash (default)
    to [
      {
        full: 'to_user@email.com',
        email: 'to_user@email.com',
        token: 'to_user',
        host: 'email.com',
        name: nil
      }
    ]
    from(
      token: 'from_user',
      host: 'email.com',
      email: 'from_email@email.com',
      full: 'From User <from_user@email.com>',
      name: 'From User'
    )
    subject 'email subject'
    body '[d] Did a thing\n[t] Doing a thing\n[b] Blocked by a thing'
    headers {'Message-ID <98984d@Robs-MacBook-Air.local.mail>'}
  end
end

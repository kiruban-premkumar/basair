FactoryBot.define do
  factory :event do
    type ""
    team nil
    user_name "MyString"
    user_id nil
    event_name "MyString"
    event_body "MyText"
    event_id "MyString"
    event_data ""
    event_time "2017-11-07 10:26:22"
  end
end

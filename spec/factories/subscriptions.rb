FactoryBot.define do
  factory :subscription do
    account nil
    plan_id 'free'
    stripe_customer_id 'MyString'
    start '2017-10-19 14:16:16'
    status 'active'
    stripe_subscription_id 'MyString'
    stripe_token 'MyString'
    card_last4 'MyString'
    card_expiration 'MyString'
    card_type 'MyString'
    stripe_status 'MyString'
    idempotency_key 'MyString'
  end
end

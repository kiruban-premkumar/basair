FactoryBot.define do
  factory :team do
    name "MyString"
    account
    timezone "Arizona"
    has_reminder false
    has_recap false
    hash_id "MyString"
    reminder_time "2017-08-28 23:10:10"
    recap_time "2017-08-28 23:10:10"
  end
end

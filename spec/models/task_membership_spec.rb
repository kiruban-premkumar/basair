require 'rails_helper'

RSpec.describe TaskMembership, type: :model do
   it "has a valid factory" do
    expect(FactoryBot.build(:task_membership)).to be_valid
  end

  it { is_expected.to belong_to(:standup) }
  it { is_expected.to belong_to(:task) }
end

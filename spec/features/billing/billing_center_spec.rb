require 'rails_helper'

RSpec.feature 'BillingCenter', type: :feature do
  login_admin
  let(:plan) { @stripe_test_helper.create_plan(id: 'free', amount: 0) }

  before(:each) do
    PaymentServices::Stripe::Subscription::CreationService.(
      user: @admin,
      account: @admin.account,
      plan: plan.id
    )
    @admin.reload
  end

  it 'should display basic plan information' do
    visit billing_index_path

    expect(current_path).to eql(billing_index_path)
    expect(page).to have_content(plan.id.humanize)
    expect(page).to have_content('Users: 1/2')
  end

  it 'should display charges' do
    charge = Stripe::Charge.create(
      amount: 999,
      currency: 'usd',
      customer: @admin.account.subscription.stripe_customer_id
    )

    visit billing_index_path

    expect(current_path).to eql(billing_index_path)
    expect(page)
      .to_not have_content('There are no charges on this account at this time')
    expect(page).to have_content(Money.new(charge.amount).format)
  end

  it 'should allow a plan to be changed' do
    starter = @stripe_test_helper.create_plan(id: 'starter', amount: 999)

    visit billing_index_path

    click_on 'Change Plan'

    find(:xpath, "//a[@href='/plans/#{starter.id}']").click

    find('#stripeToken', visible: false)
      .set @stripe_test_helper.generate_card_token
    find('#cardholder-name').set 'Test Holder'

    click_button 'Submit Payment'

    visit billing_index_path

    expect(page).to have_content('Change Card')
  end
end

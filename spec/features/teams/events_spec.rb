require 'rails_helper'

RSpec.feature "Teams", type: :feature do
  login_admin

  before(:each) do
    @json = JSON.parse(File.read('spec/fixtures/github_events/push_event.json'))
    @team = FactoryBot.create(:team, account_id: @admin.account.id)
    @json['commits'].first['timestamp'] = Time.now.iso8601
    Webhooks::Github::PushEvent::Record.perform_now(@json, @team.hash_id)
    FactoryBot.create(
      :integration,
      type: 'Integrations::Github',
      account_id: @admin.account.id,
      settings: { token: '<< your token >>' }
    )
  end

  it "should display a team's events" do
    visit team_path(@team)

    expect(page).to have_content("Events")
    expect(page).to have_content("#{@json['commits'].first['message']}")
  end
end

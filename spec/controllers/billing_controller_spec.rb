require 'rails_helper'

RSpec.describe BillingController, type: :controller do
  login_admin

  describe 'GET #index' do
    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
    end

    it 'returns http success with Stripe exception caught' do
      StripeMock.prepare_error(
        Stripe::InvalidRequestError.new('s', {}),
        :get_charges
      )
      get :index
      expect(response).to have_http_status(:success)
      expect(assigns(:charges)).to eq([])
    end
  end
end

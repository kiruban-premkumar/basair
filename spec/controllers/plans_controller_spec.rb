require 'rails_helper'

RSpec.describe PlansController, type: :controller do
  login_admin

  describe 'GET #index' do
    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
      expect(assigns(:plans).count).to eq(3)
    end
  end

  describe 'GET #show' do
    let(:token) { @stripe_test_helper.generate_card_token }
    let(:customer) do
      Stripe::Customer.create(
        email: 'johnny@appleseed.com',
        card: token
      )
    end
    let(:plan) { @stripe_test_helper.create_plan(id: 'free', amount: 0) }
    before(:each) do
      PaymentServices::Stripe::Subscription::CreationService.(
        user: @admin,
        account: @admin.account,
        plan: plan.id
      )
      @admin.reload
    end

    it 'returns http success free' do
      get :show, params: {id: 'free'}
      expect(response).to have_http_status(:redirect)
      expect(flash[:notice]).to eq 'Plan was updated successfully.'
    end

    it 'returns http success starter' do
      get :show, params: {id: 'starter'}
      expect(response).to have_http_status(:success)
      expect(response).to render_template('show')
    end

    it 'returns http success starter token' do
      @stripe_test_helper.create_plan(id: 'starter', amount: 10)
      @admin.reload
      cus =
        Stripe::Customer
        .retrieve(@admin.account.subscription.stripe_customer_id)
      card = cus.sources.create(source: @stripe_test_helper.generate_card_token)
      @admin.account.subscription.update(stripe_token: card.id)
      get :show, params: {id: 'starter'}
      expect(response).to have_http_status(:redirect)
      expect(flash[:notice]).to eq 'Plan was updated successfully.'
    end

    it 'returns http success free no subscription' do
      @admin.account.subscription.update(plan_id: nil)
      get :show, params: {id: 'free'}
      expect(response).to have_http_status(:redirect)
      expect(flash[:notice]).to eq 'Plan was updated successfully.'
    end

    it 'returns http success free no subscription error' do
      StripeMock.prepare_error(
        Stripe::StripeError.new('s', {}),
        :create_subscription
      )

      @admin.account.subscription.update(plan_id: nil)
      get :show, params: {id: plan.id}
      expect(response).to have_http_status(:redirect)
      expect(flash[:notice]).to eq 'Plan was unable to be updated!'
    end
  end
end

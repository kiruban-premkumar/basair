require 'rails_helper'

RSpec.describe ActivityController, type: :controller do
  login_user

  describe "GET #mine" do
    it "returns http success" do
      get :mine
      expect(response).to have_http_status(:success)
    end

    it 'pages the standups' do
      7.times.each do |i|
        Standup.create(
          user_id: @user.id,
          standup_date: (1+i).days.ago.iso8601
        )
      end
      get :mine
      expect(assigns(:standups).size).to eq 6
    end

    it "redirects to sign_in due to inactive plan" do
      @user.account.subscription.update(status: 'canceled')
      get :mine
      expect(response).to redirect_to root_path
    end

    it "redirects billing center due to inactive plan" do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @user = FactoryBot.create(:user)
      @user.add_role :admin
      sign_in @user
      @user.account.subscription.update(status: 'canceled')
      get :mine
      expect(response).to redirect_to billing_index_path
    end
  end

  describe "GET #feed" do
    it "returns http success" do
      get :feed
      expect(response).to have_http_status(:success)
    end
  end

end

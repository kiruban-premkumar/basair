require 'rails_helper'

RSpec.describe IntegrationsController, type: :controller do
  login_admin

  describe 'GET #index' do
    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
      expect(assigns(:grepos)).to eq(nil)
    end

    it 'returns http success with Stripe exception caught' do
      FactoryBot.create(
        :integration,
        type: 'Integrations::Github',
        account_id: @admin.account.id,
        settings: { token: '<< your token >>' }
      )
      VCR.use_cassette('github/repos') do
        get :index
        expect(response).to have_http_status(:success)
        expect(assigns(:github)).to be_truthy
        expect(assigns(:grepos)).to be_truthy
      end
    end
  end

  describe 'Delete #destroy' do
    it 'returns http success' do
      FactoryBot.create(
        :integration,
        type: 'Integrations::Github',
        account_id: @admin.account.id
      )
      delete :destroy, params: {provider: 'github'}
      expect(response).to have_http_status(:redirect)
      expect(flash[:notice]).to eq "Removed Github integration. Please besure \
to remove the application through your settings area in Github"
    end

    it 'does not delete integration' do
      FactoryBot.create(
        :integration,
        type: 'Integrations::Github',
        account_id: @admin.account.id
      )
      delete :destroy, params: {provider: '!github'}
      expect(response).to have_http_status(:redirect)
      expect(flash[:notice]).to eq 'Unable to removed !github integration'
    end
  end
end

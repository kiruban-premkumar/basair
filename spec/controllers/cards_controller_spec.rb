require 'rails_helper'

RSpec.describe CardsController, type: :controller do
  login_admin

  describe 'POST #create' do
    it 'returns http success on failure' do
      post :create, params: {
        stripeToken: @stripe_test_helper.generate_card_token
      }
      expect(response).to have_http_status(:redirect)
      expect(flash[:notice]).to eq 'Card was not successfully updated.'
    end

    it 'returns http success on success' do
      plan = @stripe_test_helper.create_plan(id: 'free', amount: 0)
      PaymentServices::Stripe::Subscription::CreationService.(
        user: @admin,
        account: @admin.account,
        plan: plan.id
      )
      @admin.reload
      cus =
        Stripe::Customer
        .retrieve(@admin.account.subscription.stripe_customer_id)
      card = cus.sources.create(source: @stripe_test_helper.generate_card_token)
      @admin.account.subscription.update(stripe_token: card.id)
      post :create, params: {
        stripeToken: @stripe_test_helper.generate_card_token
      }
      expect(response).to have_http_status(:redirect)
      expect(flash[:notice]).to eq 'Card was successfully updated.'
    end
  end
end

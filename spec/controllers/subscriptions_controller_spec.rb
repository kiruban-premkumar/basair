require 'rails_helper'

RSpec.describe SubscriptionsController, type: :controller do
  login_admin
  let(:token) { @stripe_test_helper.generate_card_token }
  let(:customer) do
    Stripe::Customer.create(
      email: 'johnny@appleseed.com',
      card: token
    )
  end
  let(:plan) { @stripe_test_helper.create_plan(id: 'free', amount: 0) }

  describe 'POST #create' do
    it 'returns http failure path' do
      post :create, params: {
        plan: 'starter',
        stripeToken: token
      }
      expect(response).to have_http_status(:redirect)
      expect(flash[:notice]).to eq 'Plan was not able to be added!'
    end

    it 'returns http success success path' do
      PaymentServices::Stripe::Subscription::CreationService.(
        user: @admin,
        account: @admin.account,
        plan: plan.id
      )
      @stripe_test_helper.create_plan(id: 'starter', amount: 10)
      post :create, params: {
        plan: 'starter',
        stripeToken: token
      }
      expect(response).to have_http_status(:redirect)
      expect(flash[:notice]).to eq 'Plan was successfully added!'
    end
  end

  describe 'DELETE #destroy' do
    it 'returns http success failure path' do
      delete :destroy, params: {
        id: @admin.account.subscription.stripe_subscription_id
      }
      expect(response).to have_http_status(:redirect)
      expect(flash[:notice]).to eq 'Plan unable to be canceled.'
    end
  end

  describe 'DELETE #destroy' do
    it 'returns http success success path' do
      resp = ::PaymentServices::Stripe::Subscription::CreateSubscription.(
        customer: customer,
        account: @admin.account,
        plan: plan.id
      )
      delete :destroy, params: {id: resp.subscription.id}
      expect(response).to have_http_status(:redirect)
      expect(flash[:notice]).to eq 'Plan has been canceled.'
    end
  end
end

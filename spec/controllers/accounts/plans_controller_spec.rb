require 'rails_helper'

RSpec.describe Accounts::PlansController, type: :controller do
  login_admin

  describe 'GET #index' do
    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
      expect(assigns(:plans).count).to eq(3)
    end
  end

  describe 'GET #show' do
    it 'returns http success' do
      get :show, params: {id: 'free'}
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to new_user_registration_path(plan: 'free')
    end
  end
end

require 'rails_helper'

RSpec.describe Events::GithubController, type: :controller do
  let(:team){ FactoryBot.create(:team) }

  describe 'POST #create' do
    it 'signature passes' do
      token = 'sha1=' + OpenSSL::HMAC.hexdigest(
        OpenSSL::Digest.new('sha1'),
        team.hash_id,
        '{}'
      )
      request.headers['HTTP_X_HUB_SIGNATURE'] = token
      request.env['RAW_POST_DATA'] = {}.to_json
      post :create, params: { team_id: team.hash_id }
      expect(response).to have_http_status(:success)
    end

    it 'signature fails' do
      token = 'sha1=' + OpenSSL::HMAC.hexdigest(
        OpenSSL::Digest.new('sha1'),
        team.hash_id,
        '23rtegf'
      )
      request.headers['HTTP_X_HUB_SIGNATURE'] = token
      request.env['RAW_POST_DATA'] = {}.to_json
      post :create, params: { team_id: team.hash_id }
      expect(response).to have_http_status(:internal_server_error)
    end

    it 'triggers push event' do
      token = 'sha1=' + OpenSSL::HMAC.hexdigest(
        OpenSSL::Digest.new('sha1'),
        team.hash_id,
        '{}'
      )
      request.headers['HTTP_X_HUB_SIGNATURE'] = token
      request.headers['HTTP_X_GITHUB_EVENT'] = 'push'
      request.env['RAW_POST_DATA'] = {}.to_json
      post :create, params: { team_id: team.hash_id }
      expect(response).to have_http_status(:success)
    end

    it 'triggers pull request event' do
      token = 'sha1=' + OpenSSL::HMAC.hexdigest(
        OpenSSL::Digest.new('sha1'),
        team.hash_id,
        '{}'
      )
      request.headers['HTTP_X_HUB_SIGNATURE'] = token
      request.headers['HTTP_X_GITHUB_EVENT'] = 'pull_request'
      request.env['RAW_POST_DATA'] = {}.to_json
      post :create, params: { team_id: team.hash_id }
      expect(response).to have_http_status(:success)
    end
  end
end

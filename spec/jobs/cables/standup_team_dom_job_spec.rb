require 'rails_helper'
include ActiveJob::TestHelper

RSpec.describe Cables::StandupTeamDomJob do
  before(:each) do
    @user = FactoryBot.create(:user)
    @team = FactoryBot.create(
      :team,
      user_ids: [@user.id],
      has_reminder: true,
      reminder_time: Time.at(
        Time.now.utc.to_i - (Time.now.utc.to_i % 15.minutes)
      ).utc
    )
    @team.update(
      days: [DaysOfTheWeekMembership.new(
        team_id: @team.id,
        day: Time.now.utc.strftime('%A').downcase
      )]
    )
    @standup = FactoryBot.create(
      :standup,
      standup_date: Date.today.iso8601,
      user_id: @user.id
    )
  end

  it 'matches with enqueued job' do
    ActiveJob::Base.queue_adapter = :test
    Cables::StandupTeamDomJob.perform_later
    expect(Cables::StandupTeamDomJob).to have_been_enqueued
  end

  it 'enqueues a default based job' do
    ActiveJob::Base.queue_adapter = :test
    expect {  Cables::StandupTeamDomJob.perform_later(@standup) }
      .to have_enqueued_job.on_queue('default')
  end

  it 'renders a partial' do
    expect(ApplicationController).to receive(:render)
    Cables::StandupTeamDomJob.perform_now(@standup)
  end
end

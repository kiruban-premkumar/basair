require 'rails_helper'
include ActiveJob::TestHelper

RSpec.describe Payments::CustomerSubscriptionDeleted do
  let(:customer) do
    Stripe::Customer.create(
      email: 'johnny@appleseed.com'
    )
  end
  let(:plan) { @stripe_test_helper.create_plan(id: 'free', amount: 0) }

  before(:each) do
    @admin = FactoryBot.create(:user, email: 'awesome@dabomb.com')
    PaymentServices::Stripe::Subscription::CreationService.(
      user: @admin,
      account: @admin.account,
      plan: plan.id
    )
    @admin.reload
    @event = StripeMock.mock_webhook_event(
      'customer.subscription.deleted',
      customer: @admin.account.subscription.stripe_customer_id,
      id: @admin.account.subscription.stripe_subscription_id
    )
  end

  it 'matches with enqueued job' do
    ActiveJob::Base.queue_adapter = :test
    Payments::CustomerSubscriptionDeleted.perform_later
    expect(Payments::CustomerSubscriptionDeleted).to have_been_enqueued
  end

  it 'enqueues a default based job' do
    ActiveJob::Base.queue_adapter = :test
    expect {  Payments::CustomerSubscriptionDeleted.perform_later(@event.id) }
      .to have_enqueued_job.on_queue('default')
  end

  it 'updates a subscription' do
    expect_any_instance_of(Subscription).to receive(:update!)
    Payments::CustomerSubscriptionDeleted.perform_now(@event.id)
  end

  it 'doesn\'t update a subscription' do
    @event = StripeMock.mock_webhook_event(
      'customer.subscription.deleted',
      customer: @admin.account.subscription.stripe_customer_id,
      id: 'fake'
    )
    expect_any_instance_of(Subscription).to_not receive(:update!)
    Payments::CustomerSubscriptionDeleted.perform_now(@event.id)
  end


end

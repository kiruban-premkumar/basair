require 'rails_helper'
include ActiveJob::TestHelper

RSpec.describe Analytics::Mixpanel::SendEventJob do
  before(:each) do
    ActiveJob::Base.queue_adapter = :test
    @user = FactoryBot.create(:user)
  end

  it 'matches with enqueued job' do
    ActiveJob::Base.queue_adapter = :test
    Analytics::Mixpanel::SendEventJob.perform_later
    expect(Analytics::Mixpanel::SendEventJob).to have_been_enqueued
  end

  it 'enqueues a default based job' do
    ActiveJob::Base.queue_adapter = :test
    expect { Analytics::Mixpanel::SendEventJob.perform_later(@user, '', {}) }
      .to have_enqueued_job.on_queue('default')
  end

  it 'recieves a track invocation onto the Tracker' do
    expect(TRACKER).to receive(:track)
    Analytics::Mixpanel::SendEventJob.perform_now(@user, '', {})
  end
end

require 'rails_helper'

describe Events::Stripe::InvoicePaymentSucceeded do
  it 'queues the mailer based on the event' do
    ActiveJob::Base.queue_adapter = :test
    expect do
      Events::Stripe::InvoicePaymentSucceeded.new.call(
        id: 1, type: 'invoice.payment_succeeded'
      )
    end.to have_enqueued_job.on_queue('mailers')

  end
end

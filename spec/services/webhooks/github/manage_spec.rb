require 'rails_helper'

describe Webhooks::Github::Manage do
  subject(:manage) { Webhooks::Github::Manage }

  describe 'create an accounts' do
    context 'from form input' do
      let(:team) do
        FactoryBot.create(
          :team,
          integration_settings: {
            github_repos: ['test|one']
          }
        )
      end


      it 'adds a webhook' do
        team.integration_settings = {
          github_repos: ['test|one', 'test|two']
        }
        expect(
          Webhooks::Github::Add
        ).to receive(:perform_later).once
        manage.(team)
      end

      it 'removes a webhook' do
        team.integration_settings = {
          github_repos: []
        }
        expect(
          Webhooks::Github::Remove
        ).to receive(:perform_later).once
        manage.(team)
      end
    end
  end
end

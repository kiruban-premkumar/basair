require 'rails_helper'

describe EmailProcessor do
  subject(:email_processor) { EmailProcessor }
  let(:user) { FactoryBot.create(:user) }
  let(:email) do
    FactoryBot.build(:email,
      to: [
        {
          email: "standup.#{user.hash_id}@app.buildasaasappinrails.com",
          token: "standup.#{user.hash_id}@app.buildasaasappinrails.com"
        }
      ]
    )
  end

  describe 'processes incoming email' do

    it 'works as intended' do
      expect { email_processor.new(email).process }
        .to change(Standup, :count).by(1)
    end

    it 'fails on bad to' do
      bad_to = FactoryBot.build(
        :email,
        to: [{ token: nil, email: 'standup@app.buildasaasappinrails.com' }]
      )
      expect { email_processor.new(bad_to).process }
        .to change(Standup, :count).by(0)
    end

    it 'fails on no user' do
      bad_to = FactoryBot.build(
        :email,
        to: [
          {
            token: 'standup.o8yhiukj@app.buildasaasappinrails.com',
            email: 'standup.o8yhiukj@app.buildasaasappinrails.com'
          }
        ]
      )
      expect { email_processor.new(bad_to).process }
        .to change(Standup, :count).by(0)
    end

    it 'only saves one per message-id' do
      expect do
        email_processor.new(email).process
        email_processor.new(email).process
      end.to change(Standup, :count).by(1)
    end

    it 'only saves one per date' do
      email2 = FactoryBot.build(:email, headers: { 'message-id': '123' })
      expect do
        email_processor.new(email).process
        email_processor.new(email2).process
      end.to change(Standup, :count).by(1)
    end

    it 'fails on empty or bad body' do
      email = FactoryBot.build(:email, body: '90ioqwhdk.qhdu')
      email2 = FactoryBot.build(:email, body: '')
      expect do
        email_processor.new(email).process
        email_processor.new(email2).process
      end.to change(Standup, :count).by(0)
    end
  end
end

require 'rails_helper'
require 'stripe_sync/plans'

describe StripeSync::Plans do

  describe 'create plans' do
    it 'creates plans' do
      expect(Stripe::Plan).to receive(:create).exactly(3).times
      StripeSync::Plans.sync!
    end

    it 'updates through delete plans' do
      @stripe_test_helper.create_plan(id: 'free', amount: 0)
      StripeSync::Plans.sync!
    end
  end
end

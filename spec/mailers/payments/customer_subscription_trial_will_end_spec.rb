require 'rails_helper'
include ActiveJob::TestHelper

RSpec.describe Payments::CustomerSubscriptionTrialWillEnd, type: :mailer do
  let(:customer) do
    Stripe::Customer.create(
      email: 'johnny@appleseed.com'
    )
  end
  let(:plan) { @stripe_test_helper.create_plan(id: 'free', amount: 0) }

  before(:each) do
    @admin = FactoryBot.create(:user, email: 'awesome@dabomb.com')
    PaymentServices::Stripe::Subscription::CreationService.(
      user: @admin,
      account: @admin.account,
      plan: plan.id
    )
    @admin.reload
    @event = StripeMock.mock_webhook_event(
      'customer.subscription.trial_will_end',
      customer: @admin.account.subscription.stripe_customer_id,
      id: @admin.account.subscription.stripe_subscription_id
    )
  end

  it 'job is created' do
    ActiveJob::Base.queue_adapter = :test
    expect do
      Payments::CustomerSubscriptionTrialWillEnd.email(@event.id).deliver_later
    end.to have_enqueued_job.on_queue('mailers')
  end

  it 'email is sent free' do
    expect do
      perform_enqueued_jobs do
        Payments::CustomerSubscriptionTrialWillEnd.email(
          @event.id
        ).deliver_later
      end
    end.to change { ActionMailer::Base.deliveries.size }.by(1)
  end

  it 'email is sent paid' do
    @admin.account.subscription.update(plan_id: 'not_free')
    event = StripeMock.mock_webhook_event(
      'customer.subscription.trial_will_end',
      customer: @admin.account.subscription.stripe_customer_id,
      id: @admin.account.subscription.stripe_subscription_id,
      plan: plan.id,
      items: {data: [{plan: plan}]}
    )
    expect do
      perform_enqueued_jobs do
        Payments::CustomerSubscriptionTrialWillEnd.email(
          event.id
        ).deliver_later
      end
    end.to change { ActionMailer::Base.deliveries.size }.by(1)
  end

  it 'email is not sent' do
    expect do
      @evt_id = StripeMock.mock_webhook_event(
        'customer.subscription.trial_will_end'
      ).id
      perform_enqueued_jobs do
        Payments::CustomerSubscriptionTrialWillEnd.email(@evt_id).deliver_later
      end
    end.to change { ActionMailer::Base.deliveries.size }.by(0)
  end

  it 'email is sent to the right user' do
    perform_enqueued_jobs do
      Payments::CustomerSubscriptionTrialWillEnd.email(@event.id).deliver_later
    end

    mail = ActionMailer::Base.deliveries.last
    expect(mail.to[0]).to eq @admin.email
  end

end

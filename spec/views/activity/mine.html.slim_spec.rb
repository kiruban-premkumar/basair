require 'rails_helper'

RSpec.describe 'activity/mine.html.slim', type: :view do
  before do
    allow(view)
      .to receive(:current_date)
      .and_return(Date.today.strftime('%a %d %b %Y'))
    user = User.create(email: "t@test.co")
    7.times.each do |i|
      Standup.create(
        user_id: user.id,
        standup_date: (1+i).days.ago.iso8601
      )
    end
    standups = Standup.where(user_id: user.id).page(1).per(6)
    assign(:standups, standups)
  end

  it 'renders the word mine' do
    render template: 'activity/mine.html.slim'
    expect(rendered).to match(/My Activity/)
  end
end
